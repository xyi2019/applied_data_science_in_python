import nltk
import pandas as pd
import numpy as np

# If you would like to work with the raw text you can use 'moby_raw'
with open('moby.txt', 'r') as f:
    moby_raw = f.read()

# If you would like to work with the novel in nltk.Text format you can use 'text1'
moby_tokens = nltk.word_tokenize(moby_raw)
print("n_tokens = {}".format(len(moby_tokens)))

text1 = nltk.Text(moby_tokens)
print('type of text1 is{}'.format(type(text1)))
print("the length of text1 is {}".format(len(text1)))
tags = nltk.pos_tag(text1)

# Compute the word frquency
dist = nltk.FreqDist(moby_tokens)
print("the type of dist is {}".format(type(dist)))

from nltk.corpus import words
correct_spellings = words.words()

def answer_eight():
    dict_postag = {}
    sents = nltk.sent_tokenize(moby_raw)
    for sent in sents:
        leafs = nltk.pos_tag(sent)
        for leaf in leafs:
            if dict_postag.get(leaf[1]) is None:
                dict_postag[leaf[1]] = 1
            else:
                dict_postag[leaf[1]] += 1

    # Find the top five most frequent tags
    tuples = sorted(dict_postag.items(), key=lambda x: x[1], reverse=True)
    top_5 = tuples[:5]
    return top_5

answer_eight()

def answer_nine(entries=['cormulent', 'incendenece', 'validrate']):
    import sys
    from nltk.metrics.distance import jaccard_distance
    recommend = []
    for entry in entries:
        min_distance = sys.maxsize
        correct_word = ''
        for word in correct_spellings:
            set_entry = set([entry[i:i + 3] for i in range(len(entry) - 2)])
            set_word = set([word[i:i + 3] for i in range(len(word) - 2)])
            distance = jaccard_distance(set_entry, set_word)
            if distance < min_distance:
                min_distance = distance
                correct_word = word

        # Add the word with the shortest distance
        recommend.append(correct_word)

    return recommend  # Your answer here

# answer_nine()


