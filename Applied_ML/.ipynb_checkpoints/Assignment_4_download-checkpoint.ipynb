{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "_You are currently looking at **version 1.0** of this notebook. To download notebooks and datafiles, as well as get help on Jupyter notebooks in the Coursera platform, visit the [Jupyter Notebook FAQ](https://www.coursera.org/learn/python-machine-learning/resources/bANLa) course resource._\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assignment 4 - Understanding and Predicting Property Maintenance Fines\n",
    "\n",
    "This assignment is based on a data challenge from the Michigan Data Science Team ([MDST](http://midas.umich.edu/mdst/)). \n",
    "\n",
    "The Michigan Data Science Team ([MDST](http://midas.umich.edu/mdst/)) and the Michigan Student Symposium for Interdisciplinary Statistical Sciences ([MSSISS](https://sites.lsa.umich.edu/mssiss/)) have partnered with the City of Detroit to help solve one of the most pressing problems facing Detroit - blight. [Blight violations](http://www.detroitmi.gov/How-Do-I/Report/Blight-Complaint-FAQs) are issued by the city to individuals who allow their properties to remain in a deteriorated condition. Every year, the city of Detroit issues millions of dollars in fines to residents and every year, many of these fines remain unpaid. Enforcing unpaid blight fines is a costly and tedious process, so the city wants to know: how can we increase blight ticket compliance?\n",
    "\n",
    "The first step in answering this question is understanding when and why a resident might fail to comply with a blight ticket. This is where predictive modeling comes in. For this assignment, your task is to predict whether a given blight ticket will be paid on time.\n",
    "\n",
    "All data for this assignment has been provided to us through the [Detroit Open Data Portal](https://data.detroitmi.gov/). **Only the data already included in your Coursera directory can be used for training the model for this assignment.** Nonetheless, we encourage you to look into data from other Detroit datasets to help inform feature creation and model selection. We recommend taking a look at the following related datasets:\n",
    "\n",
    "* [Building Permits](https://data.detroitmi.gov/Property-Parcels/Building-Permits/xw2a-a7tf)\n",
    "* [Trades Permits](https://data.detroitmi.gov/Property-Parcels/Trades-Permits/635b-dsgv)\n",
    "* [Improve Detroit: Submitted Issues](https://data.detroitmi.gov/Government/Improve-Detroit-Submitted-Issues/fwz3-w3yn)\n",
    "* [DPD: Citizen Complaints](https://data.detroitmi.gov/Public-Safety/DPD-Citizen-Complaints-2016/kahe-efs3)\n",
    "* [Parcel Map](https://data.detroitmi.gov/Property-Parcels/Parcel-Map/fxkw-udwf)\n",
    "\n",
    "___\n",
    "\n",
    "We provide you with two data files for use in training and validating your models: train.csv and test.csv. Each row in these two files corresponds to a single blight ticket, and includes information about when, why, and to whom each ticket was issued. The target variable is compliance, which is True if the ticket was paid early, on time, or within one month of the hearing data, False if the ticket was paid after the hearing date or not at all, and Null if the violator was found not responsible. Compliance, as well as a handful of other variables that will not be available at test-time, are only included in train.csv.\n",
    "\n",
    "Note: All tickets where the violators were found not responsible are not considered during evaluation. They are included in the training set as an additional source of data for visualization, and to enable unsupervised and semi-supervised approaches. However, they are not included in the test set.\n",
    "\n",
    "<br>\n",
    "\n",
    "**File descriptions** (Use only this data for training your model!)\n",
    "\n",
    "    train.csv - the training set (all tickets issued 2004-2011)\n",
    "    test.csv - the test set (all tickets issued 2012-2016)\n",
    "    addresses.csv & latlons.csv - mapping from ticket id to addresses, and from addresses to lat/lon coordinates. \n",
    "     Note: misspelled addresses may be incorrectly geolocated.\n",
    "\n",
    "<br>\n",
    "\n",
    "**Data fields**\n",
    "\n",
    "train.csv & test.csv\n",
    "\n",
    "    ticket_id - unique identifier for tickets\n",
    "    agency_name - Agency that issued the ticket\n",
    "    inspector_name - Name of inspector that issued the ticket\n",
    "    violator_name - Name of the person/organization that the ticket was issued to\n",
    "    violation_street_number, violation_street_name, violation_zip_code - Address where the violation occurred\n",
    "    mailing_address_str_number, mailing_address_str_name, city, state, zip_code, non_us_str_code, country - Mailing address of the violator\n",
    "    ticket_issued_date - Date and time the ticket was issued\n",
    "    hearing_date - Date and time the violator's hearing was scheduled\n",
    "    violation_code, violation_description - Type of violation\n",
    "    disposition - Judgment and judgement type\n",
    "    fine_amount - Violation fine amount, excluding fees\n",
    "    admin_fee - $20 fee assigned to responsible judgments\n",
    "state_fee - $10 fee assigned to responsible judgments\n",
    "    late_fee - 10% fee assigned to responsible judgments\n",
    "    discount_amount - discount applied, if any\n",
    "    clean_up_cost - DPW clean-up or graffiti removal cost\n",
    "    judgment_amount - Sum of all fines and fees\n",
    "    grafitti_status - Flag for graffiti violations\n",
    "    \n",
    "train.csv only\n",
    "\n",
    "    payment_amount - Amount paid, if any\n",
    "    payment_date - Date payment was made, if it was received\n",
    "    payment_status - Current payment status as of Feb 1 2017\n",
    "    balance_due - Fines and fees still owed\n",
    "    collection_status - Flag for payments in collections\n",
    "    compliance [target variable for prediction] \n",
    "     Null = Not responsible\n",
    "     0 = Responsible, non-compliant\n",
    "     1 = Responsible, compliant\n",
    "    compliance_detail - More information on why each ticket was marked compliant or non-compliant\n",
    "\n",
    "\n",
    "___\n",
    "\n",
    "## Evaluation\n",
    "\n",
    "Your predictions will be given as the probability that the corresponding blight ticket will be paid on time.\n",
    "\n",
    "The evaluation metric for this assignment is the Area Under the ROC Curve (AUC). \n",
    "\n",
    "Your grade will be based on the AUC score computed for your classifier. A model which with an AUROC of 0.7 passes this assignment, over 0.75 will recieve full points.\n",
    "___\n",
    "\n",
    "For this assignment, create a function that trains a model to predict blight ticket compliance in Detroit using `train.csv`. Using this model, return a series of length 61001 with the data being the probability that each corresponding ticket from `test.csv` will be paid, and the index being the ticket_id.\n",
    "\n",
    "Example:\n",
    "\n",
    "    ticket_id\n",
    "       284932    0.531842\n",
    "       285362    0.401958\n",
    "       285361    0.105928\n",
    "       285338    0.018572\n",
    "                 ...\n",
    "       376499    0.208567\n",
    "       376500    0.818759\n",
    "       369851    0.018528\n",
    "       Name: compliance, dtype: float32"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "C:\\ProgramData\\Anaconda3\\lib\\site-packages\\IPython\\core\\interactiveshell.py:2881: DtypeWarning: Columns (11,12,31) have mixed types. Specify dtype option on import or set low_memory=False.\n",
      "  exec(code_obj, self.user_global_ns, self.user_ns)\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of compliance:  11597\n",
      "Number of not compliance:  148283\n",
      "Number of not responsible:  90426\n",
      "missing column:  violation_code_61-63.0100\n",
      "missing column:  agency_name_Health Department\n",
      "missing column:  agency_name_Neighborhood City Halls\n",
      "new columns:  set()\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "ticket_id\n",
       "284932    0.102179\n",
       "285362    0.022178\n",
       "285361    0.078210\n",
       "285338    0.078362\n",
       "285346    0.101190\n",
       "285345    0.078362\n",
       "285347    0.104023\n",
       "285342    0.178260\n",
       "285530    0.035140\n",
       "284989    0.036955\n",
       "285344    0.077931\n",
       "285343    0.035874\n",
       "285340    0.035874\n",
       "285341    0.077931\n",
       "289828    0.044656\n",
       "289830    0.060057\n",
       "289829    0.049097\n",
       "292133    0.035874\n",
       "292134    0.077931\n",
       "285349    0.101190\n",
       "285348    0.078362\n",
       "284991    0.036955\n",
       "285532    0.043750\n",
       "286073    0.043750\n",
       "285406    0.027703\n",
       "285001    0.091155\n",
       "285006    0.060606\n",
       "365862    0.962383\n",
       "285405    0.022178\n",
       "287857    0.021671\n",
       "            ...   \n",
       "376276    0.023329\n",
       "376218    0.037487\n",
       "376368    0.060057\n",
       "376369    0.078362\n",
       "376225    0.037487\n",
       "376222    0.028123\n",
       "376362    0.038966\n",
       "376363    0.062196\n",
       "376228    0.058490\n",
       "376265    0.037487\n",
       "376286    0.295659\n",
       "376320    0.040796\n",
       "376314    0.038050\n",
       "376327    0.416189\n",
       "376435    0.066088\n",
       "376434    0.082972\n",
       "376459    0.084798\n",
       "376478    0.021111\n",
       "376473    0.040796\n",
       "376484    0.038269\n",
       "376482    0.030176\n",
       "376480    0.024835\n",
       "376479    0.024835\n",
       "376481    0.024835\n",
       "376483    0.024397\n",
       "376496    0.023067\n",
       "376497    0.023067\n",
       "376499    0.080736\n",
       "376500    0.080736\n",
       "369851    0.249617\n",
       "Name: compliance, Length: 61001, dtype: float32"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "pd.set_option(\"display.max_columns\", 40)\n",
    "\n",
    "#%matplotlib inline\n",
    "\n",
    "def get_train_set(debug_mode = False):    \n",
    "    # Your code here        \n",
    "    df = pd.read_csv(\"train.csv\", encoding = 'latin1')    \n",
    "    \n",
    "    if debug_mode:        \n",
    "        #print(df.columns)        \n",
    "        n_compliance = len(df[df[\"compliance\"] == 1])\n",
    "        n_not_compliance = len(df[df[\"compliance\"] == 0])\n",
    "        n_not_responsible = len(df[df[\"compliance\"].isnull()])   \n",
    "        print(\"Number of compliance: \", n_compliance)\n",
    "        print(\"Number of not compliance: \", n_not_compliance)\n",
    "        print(\"Number of not responsible: \", n_not_responsible)        \n",
    "        \n",
    "        ## Check address and mailing address\n",
    "        df_address_match = df[(df[\"violation_street_number\"] == df[\"mailing_address_str_number\"]) & \n",
    "                              (df[\"violation_street_name\"] == df[\"mailing_address_str_name\"])]\n",
    "        n_address_match = df_address_match.shape[0] \n",
    "\n",
    "        n_address_match_and_paid = (df_address_match[df_address_match[\"compliance\"] == 1]).shape[0]\n",
    "                \n",
    "        #print(\"Number of agnecies: \", df[\"agency_name\"].unique())        \n",
    "        #print(\"Number of address match: \", n_address_match)        \n",
    "        #print(\"Number of address match & paid: \", n_address_match_and_paid)                \n",
    "    \n",
    "    #if debug_mode:        \n",
    "        #print(df.describe(include = \"all\"))\n",
    "        \n",
    "    return df\n",
    "\n",
    "def get_address(debug_mode = False):    \n",
    "    # Your code here    \n",
    "    df = pd.read_csv(\"addresses.csv\", encoding = 'latin1')\n",
    "    \n",
    "    #if debug_mode:\n",
    "        #print(df.columns)\n",
    "            \n",
    "    return df\n",
    "\n",
    "def get_latlon(debug_mode = False):    \n",
    "    # Your code here    \n",
    "    df = pd.read_csv(\"latlons.csv\", encoding = 'latin1')\n",
    "    \n",
    "    #if debug_mode:\n",
    "        #print(df.columns)\n",
    "            \n",
    "    return df\n",
    "\n",
    "def get_test_set(debug_mode = False):    \n",
    "    # Your code here    \n",
    "    df = pd.read_csv(\"test.csv\", encoding = 'latin1')\n",
    "    \n",
    "    #if debug_mode:\n",
    "        #print(df.columns)\n",
    "        #print(\"Lenght of ticket_id column: \", len(df[\"ticket_id\"]))                \n",
    "        #print(df.describe(include = \"all\"))\n",
    "    \n",
    "    return df\n",
    "\n",
    "def model_eval(y_true, y_calc):    \n",
    "    #import matplotlib.pyplot as plt\n",
    "    #import seaborn as sns\n",
    "    from sklearn.metrics import confusion_matrix\n",
    "    from sklearn.metrics import classification_report\n",
    "    cm = confusion_matrix(y_true, y_calc)\n",
    "    df_cm = pd.DataFrame(cm)\n",
    "    \n",
    "    ## Should be disabled before submission\n",
    "    #plt.figure(figsize=(5.5, 4))\n",
    "    #sns.heatmap(df_cm, annot = True)\n",
    "    #plt.ylabel('True label')\n",
    "    #plt.xlabel('Predicted label')\n",
    "    \n",
    "    ## Print classification_report\n",
    "    print(classification_report(y_true, y_calc))\n",
    "\n",
    "def prepare_dataset(df_in, with_compliance_column, method, debug_mode):    \n",
    "    \n",
    "    ## Get address dataset\n",
    "    df_address = get_address()\n",
    "    #if debug_mode:\n",
    "        #print (df_address)\n",
    "    \n",
    "    ## Merge df_in and df_latlon\n",
    "    df = df_in.merge(df_address)    \n",
    "    #if debug_mode:\n",
    "        #print(\"After merge with address book\")\n",
    "        #print(df[[\"ticket_id\", \"address\"]])\n",
    "   \n",
    "    ## Get latitude/lontitude dataset\n",
    "    df_latlon = get_latlon()\n",
    "    #if debug_mode:\n",
    "        #print (df_latlon)\n",
    "    \n",
    "    ## Merge def_in and df_latlon\n",
    "    n_rows = df.shape[0]\n",
    "    #print(\"n_rows_before_merge: \", n_rows )\n",
    "    df = df.merge(df_latlon)        \n",
    "    n_rows = df.shape[0]\n",
    "    if debug_mode:\n",
    "        print(\"n_rows_after_merge: \", n_rows )\n",
    "    \n",
    "    if debug_mode:\n",
    "        print(\"After merge with geocode\")\n",
    "        #print(df[[\"ticket_id\", \"lat\", \"lon\"]])\n",
    "    \n",
    "    ## Feature selections\n",
    "    if with_compliance_column:\n",
    "        df = df[[\"ticket_id\", \"agency_name\", \"lat\", \"lon\", \"violation_code\", \"discount_amount\", \"judgment_amount\", \"compliance\"]]    \n",
    "    else:\n",
    "        df = df[[\"ticket_id\", \"agency_name\", \"lat\", \"lon\", \"violation_code\", \"discount_amount\", \"judgment_amount\"]]    \n",
    "        \n",
    "    ## Replace missing entries in lat column by 42.3314 and lon by 83.0458\n",
    "    df[\"lat\"].fillna(42.3314, inplace=True)\n",
    "    df[\"lon\"].fillna(83.0458, inplace=True)\n",
    "        \n",
    "    ## Drop rows with missing entries\n",
    "    df = df.dropna(how = \"any\")        \n",
    "    n_rows = df.shape[0]\n",
    "    if debug_mode:\n",
    "        print(\"n_rows_after_dropna: \", n_rows )\n",
    "    \n",
    "    return df;\n",
    "\n",
    "def cv(X, y, method):\n",
    "    from sklearn.tree import DecisionTreeClassifier\n",
    "    from sklearn.linear_model import LogisticRegression\n",
    "    from sklearn.svm import SVC\n",
    "    from sklearn.ensemble import RandomForestClassifier\n",
    "    from sklearn.ensemble import GradientBoostingClassifier\n",
    "    from sklearn.model_selection import cross_val_score\n",
    "     \n",
    "    ## Create the classifier and fit the classifier\n",
    "    if (method == \"logistic_regression\"):\n",
    "        clf = LogisticRegression()\n",
    "    elif (method == \"support_vector_machine\"):\n",
    "        clf = SVC(kernel = \"poly\", degree = 2)\n",
    "    elif (method == \"random_forest\"):\n",
    "        clf = RandomForestClassifier()\n",
    "    elif (method == \"gradient_boosting_tree\"):\n",
    "        clf = GradientBoostingClassifier()   \n",
    "    else:\n",
    "        clf = DecisionTreeClassifier(class_weight = \"balanced\")\n",
    "    cv_scores = cross_val_score(clf, X, y, cv = 5)\n",
    "    print(\"Cross-validation results: \\n\")\n",
    "    print(cv_scores)\n",
    "    \n",
    "def auroc(X, y, method):\n",
    "    #import matplotlib.pyplot as plt\n",
    "    from sklearn.tree import DecisionTreeClassifier\n",
    "    from sklearn.linear_model import LogisticRegression\n",
    "    from sklearn.svm import SVC\n",
    "    from sklearn.ensemble import RandomForestClassifier\n",
    "    from sklearn.ensemble import GradientBoostingClassifier\n",
    "    from sklearn.metrics import roc_curve\n",
    "    from sklearn.metrics import auc\n",
    "     \n",
    "    ## Create the classifier and fit the classifier\n",
    "    if (method == \"logistic_regression\"):\n",
    "        clf = LogisticRegression()\n",
    "    elif (method == \"support_vector_machine\"):\n",
    "        clf = SVC(kernel = \"poly\", degree = 2)\n",
    "    elif (method == \"random_forest\"):\n",
    "        clf = RandomForestClassifier()\n",
    "    elif (method == \"gradient_boosting_tree\"):\n",
    "        clf = GradientBoostingClassifier()        \n",
    "    else:\n",
    "        clf = DecisionTreeClassifier(class_weight = \"balanced\")\n",
    "    \n",
    "    clf.fit(X, y)\n",
    "    probabilities = clf.predict_proba(X)\n",
    "    fpr, tpr, threshold = roc_curve(y, probabilities[:, 1])\n",
    "    area = auc(fpr, tpr)\n",
    "    #print(\"fpr: \\n\", fpr)\n",
    "    #print(\"tpr: \\n\", tpr)\n",
    "    #print(\"threshold: \\n\", threshold)        \n",
    "    print(\"auc = \", area)\n",
    "    \n",
    "    #plt.figure()\n",
    "    #plt.xlim([-0.01, 1.00])\n",
    "    #plt.ylim([-0.01, 1.01])\n",
    "    #plt.plot(fpr, tpr, lw=3, label='LogRegr ROC curve (area = {:0.2f})'.format(area))\n",
    "    #plt.xlabel('False Positive Rate', fontsize=16)\n",
    "    #plt.ylabel('True Positive Rate', fontsize=16)\n",
    "    #plt.title('ROC curve)', fontsize=16)\n",
    "    #plt.legend(loc='lower right', fontsize=13)\n",
    "    #plt.plot([0, 1], [0, 1], color='navy', lw=3, linestyle='--')\n",
    "    #plt.axes().set_aspect('equal')\n",
    "    #plt.show()    \n",
    "\n",
    "def get_correlation_matrix(df_in):\n",
    "    ## Get the numerical columns\n",
    "    import numpy as np\n",
    "    import pandas as pd\n",
    "    df = df_in.select_dtypes(include=[np.number])\n",
    "    corr_m = df.corr()\n",
    "    return corr_m\n",
    "\n",
    "def blight_model():    \n",
    "    from sklearn.tree import DecisionTreeClassifier\n",
    "    from sklearn.linear_model import LogisticRegression\n",
    "    from sklearn.svm import SVC\n",
    "    from sklearn.ensemble import RandomForestClassifier\n",
    "    from sklearn.ensemble import GradientBoostingClassifier\n",
    "    from sklearn.model_selection import GridSearchCV\n",
    "    \n",
    "    methods = [\"decision_tree\", \"logistic_regression\", \"support_vector_machine\", \"random_forest\", \"gradient_boosting_tree\"]\n",
    "    method = methods[4]\n",
    "    debug_mode = False\n",
    "\n",
    "    ## Prpepare the training dataset    \n",
    "    df_train = get_train_set(True)        \n",
    "    df = prepare_dataset(df_train, True, method, debug_mode)\n",
    "    if debug_mode:\n",
    "        print(\"using \" + method)\n",
    "        #print(\"training dataset: \\n\")\n",
    "        #print(df_train)\n",
    "\n",
    "        #print(\"prepared testing dataset: \\n\")\n",
    "        #print(df)\n",
    "\n",
    "    ## Split df into X and y\n",
    "    use_violation_code = True\n",
    "    if use_violation_code:\n",
    "        X = df[[\"agency_name\", \"lat\", \"lon\", \"violation_code\", \"discount_amount\", \"judgment_amount\"]]\n",
    "    else:\n",
    "        X = df[[\"agency_name\", \"lat\", \"lon\", \"discount_amount\", \"judgment_amount\"]]\n",
    "        \n",
    "    y = df[\"compliance\"]\n",
    "    \n",
    "    #print(X.head())\n",
    "    \n",
    "    ## One-hot coding and save the list of columns for later use\n",
    "    X = pd.get_dummies(X, columns = [\"agency_name\"])\n",
    "        \n",
    "    if use_violation_code:\n",
    "        ## One-hot coding for \"violation_code\" and save the list of columns for later use\n",
    "        violation_codes = X[\"violation_code\"].value_counts()\n",
    "        n_codes = len(violation_codes)\n",
    "        n_max = 50\n",
    "        #print(\"violations codes: \", violation_codes)\n",
    "        #print(\"n_codes = \", n_codes)\n",
    "\n",
    "        ## Only keep the n_max most frequent categories and lable the rest as \"others\"\n",
    "        X[\"violation_code\"] = X[\"violation_code\"].where(X[\"violation_code\"].isin(violation_codes[:n_max].index), \"others\")\n",
    "\n",
    "        ## One-hot coding for \"violation_code\"\n",
    "        X = pd.get_dummies(X, columns = [\"violation_code\"])\n",
    "        #print(X.head())\n",
    "        \n",
    "    columns_train = X.columns\n",
    "    #print(\"columns in the training set:\", columns_train)\n",
    "    if debug_mode:\n",
    "        #print(X)    \n",
    "        print(\"Training set summary: \", X.describe(include = \"all\"))\n",
    "\n",
    "    ## Standardize features for logistic regression\n",
    "    if (method == \"logistic_regression\"):\n",
    "        from sklearn.preprocessing import StandardScaler\n",
    "        scaler = StandardScaler()\n",
    "        #X = scaler.fit_transform(X)        \n",
    "\n",
    "    ## Create the classifier and fit the classifier\n",
    "    if (method == \"logistic_regression\"):        \n",
    "        clf = LogisticRegression()        \n",
    "    elif (method == \"support_vector_machine\"):\n",
    "        clf = SVC(kernel = \"poly\", degree = 2)        \n",
    "    elif (method == \"random_forest\"):\n",
    "        clf = RandomForestClassifier()        \n",
    "    elif (method == \"gradient_boosting_tree\"):\n",
    "        clf = GradientBoostingClassifier()        \n",
    "    else:\n",
    "        clf = DecisionTreeClassifier(class_weight = \"balanced\")\n",
    "        \n",
    "    #print(\"BEFORE FIT()\")\n",
    "    clf.fit(X, y)\n",
    "    #print(\"AFTER FIT()\")\n",
    "\n",
    "    ## Experiment\n",
    "    y_cal = clf.predict(X)\n",
    "\n",
    "    ## Score the classfier with the training set    \n",
    "    score_model = True\n",
    "    if score_model:    \n",
    "        y_score = clf.score(X, y)\n",
    "        if debug_mode:\n",
    "            print(\"y_score = \", y_score)\n",
    "\n",
    "    ## Evaluate the model trained with X_train, y_train\n",
    "    eval_model = False\n",
    "    if eval_model:\n",
    "        df[\"predict\"] = clf.predict(X)\n",
    "        n_compliance = len(df[df[\"predict\"] == 1])\n",
    "        n_not_compliance = len(df[df[\"predict\"] == 0])        \n",
    "        print(\"Number of compliance predicted: \", n_compliance)\n",
    "        print(\"Number of not compliance predicted: \", n_not_compliance)\n",
    "\n",
    "        ## Display confusion matrix\n",
    "        model_eval(y, df[\"predict\"])\n",
    "\n",
    "        ## Perform cross validation\n",
    "        cv(X, y, method)\n",
    "\n",
    "        ## Compute au-roc for the training set\n",
    "        auroc(X, y, method)    \n",
    "        \n",
    "    ## Experiment with grid search in given parameter space\n",
    "    grid_search = False\n",
    "    if grid_search:    \n",
    "        if (method == \"gradient_boosting_tree\"):\n",
    "            params = {\"n_estimators\":[50, 100, 150], \"learning_rate\":np.logspace(-2, 0, 3)}\n",
    "            gbc = GradientBoostingClassifier()\n",
    "            clf = GridSearchCV(gbc, param_grid = params, scoring = \"roc_auc\")\n",
    "            clf.fit(X, y)\n",
    "            #print(\"best results: \", clf.cv_results_)\n",
    "\n",
    "    run_test = True\n",
    "    if run_test:\n",
    "        ## Get the testing dataset\n",
    "        df_test = get_test_set(debug_mode)\n",
    "        #if debug_mode:\n",
    "            #print(df_test)\n",
    "        \n",
    "        ## Prepare the dataset\n",
    "        df = prepare_dataset(df_test, False, method, debug_mode)\n",
    "\n",
    "        #if debug_mode:        \n",
    "            #print(\"prepared testing dataset: \\n\")\n",
    "            #print(df)\n",
    "\n",
    "        ## Split df into X_test and y_test\n",
    "        if use_violation_code:\n",
    "            X_test = df[[\"agency_name\", \"lat\", \"lon\", \"violation_code\", \"discount_amount\", \"judgment_amount\"]]\n",
    "        else:\n",
    "            X_test = df[[\"agency_name\", \"lat\", \"lon\", \"discount_amount\", \"judgment_amount\"]]\n",
    "        \n",
    "        ## One-hot coding for \"agency_name\"\n",
    "        X_test = pd.get_dummies(X_test, columns = [\"agency_name\"])        \n",
    "        #if debug_mode:\n",
    "            #print(X_test)    \n",
    "            #print(\"Test set summary: \", X_test.describe(include = \"all\"))\n",
    "        \n",
    "        if use_violation_code:\n",
    "            ## Treatment for \"violation_code\"\n",
    "            ## Only keep the n_max most frequent categories and lable the rest as \"others\"\n",
    "            X_test[\"violation_code\"] = X_test[\"violation_code\"].where(X_test[\"violation_code\"].isin(violation_codes[:n_max].index), \"others\")\n",
    "\n",
    "            ## One-hot coding for \"violation_code\"\n",
    "            X_test = pd.get_dummies(X_test, columns = [\"violation_code\"])\n",
    "            #print(X_test.head())\n",
    "            \n",
    "        ## The available agency names in the test data set may be smaller than the original, or it may contain new names\n",
    "        ## that are not appeared in the original. We have to handle the possibilty here. We can derive a subclass from\n",
    "        ## pandas dataframe and implement a method for one-hot-coding\n",
    "        ## The code is based on a blog post fastml.com about pd.get_dummies\n",
    "        columns_test = X_test.columns       \n",
    "        #print(\"columns in the test set:\", columns_test)\n",
    "        columns_missing = set(columns_train) - set(columns_test)\n",
    "        for col in columns_missing:\n",
    "            print(\"missing column: \", col)\n",
    "            X_test[col] = 0     \n",
    "            \n",
    "        ## Also need remove any new categories that doesn't exist in the training set\n",
    "        columns_new =  set(columns_test) - set(columns_train)\n",
    "        print(\"new columns: \", columns_new)\n",
    "        X_test = X_test[columns_train] \n",
    "        \n",
    "        #print(X_test.head())\n",
    "        \n",
    "        ## Standardize features for logistic regression\n",
    "        if (method == \"logistic_regression\"):\n",
    "            from sklearn.preprocessing import StandardScaler            \n",
    "            scaler = StandardScaler()\n",
    "            # X_test = scaler.fit_transform(X_test)    \n",
    "\n",
    "        ## Predict label\n",
    "        predict_label = True\n",
    "        if (predict_label):\n",
    "            df[\"predict\"] = clf.predict(X_test)\n",
    "            n_compliance = len(df[df[\"predict\"] == 1])\n",
    "            n_not_compliance = len(df[df[\"predict\"] == 0])\n",
    "            if debug_mode:                \n",
    "                print(\"Number of compliance predicted: \", n_compliance)\n",
    "                print(\"Number of not compliance predicted: \", n_not_compliance)                \n",
    "\n",
    "        ## Predict probability                 \n",
    "        pm = clf.predict_proba(X_test)\n",
    "        s = pd.Series(pm[:,1], index = df[\"ticket_id\"], name = \"compliance\")\n",
    "        s = s.astype(\"float32\")\n",
    "        \n",
    "        if debug_mode:\n",
    "            print (\"type(s) = \", type(s))\n",
    "            print(\"len(s) = \", len(s))\n",
    "            print(s[(s > 0.5) & (s < 1.0)])\n",
    "\n",
    "            print(\"s[284932] = 0.531842 vs \", s[284932])\n",
    "            print(\"s[285362] = 0.401958 vs \", s[285362])\n",
    "            print(\"s[285361] = 0.105928 vs \", s[285361])\n",
    "            print(\"s[285338] = 0.018572 vs \", s[285338])\n",
    "\n",
    "            print(\"s[376499] = 0.208567 vs \", s[376499])\n",
    "            print(\"s[376500] = 0.818759 vs \", s[376500])\n",
    "            print(\"s[369851] = 0.018528 vs \", s[369851])            \n",
    "        \n",
    "        return s\n",
    "\n",
    "blight_model()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x = 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "coursera": {
   "course_slug": "python-machine-learning",
   "graded_item_id": "nNS8l",
   "launcher_item_id": "yWWk7",
   "part_id": "w8BSS"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
